var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');
var mc = mongodb.MongoClient;
var ObjectID = mongodb.ObjectID;

var messagesCollection;
var usersCollection;


//connect to the mongo database and set up the collections
var connectToDBs = function(callback) {
    mc.connect('mongodb://localhost/chat-server', function(err, db) {
        if (err) {
            throw err;
        }

        messagesCollection = db.collection('messages');
        usersCollection = db.collection('users');

        if (callback) {
            callback();
        }
    });
}

connectToDBs();

//send array of all posted messages to the client
router.get('/getMessages', function(req, res) {
	var renderMessages = function(err, notes) {
	    if (err) {
	        notes = [{"sender": "App", "content": "ERROR FETCHING MESSAGES"}];
	    }
	    res.send(notes);
	}

	messagesCollection.find().toArray(renderMessages);

});

router.post('/postMessage', function(req, res) {
	var username = req.body.username;
	var content = req.body.content;

	var reportInserted = function(err, messagesInserted) {
        if (err) {
            res.send("ERROR: Could not create a new message");
        } else {
            res.send(messagesInserted[0]);
        }
    }

	if (username) {
        if (content) {
        	var newMessage = {"sender": username, "content": content};
            messagesCollection.insert(newMessage, reportInserted);
        } else {
            res.send("ERROR: bad parameters");
        }
    } else {
        res.send("ERROR: not logged in");
    }
});

router.post('/joinChat', function(req, res){
	var username = req.body.username;
	var password = req.body.password;

	var newUserAdded = function(err, usersInserted){
		if (err) {
            res.send("ERROR: Could not create a new user");
        } else {
            res.send(usersInserted[0]);
        }
	}

	var checkUsername = function(err, user) {
		if (err) {
	    	res.send("ERROR: unable to check username");
		} else if (user === null) {
			//no user matches given username therefore make a new user.
			var newUser = {"username": username.toLowerCase(), "password": password}
			usersCollection.insert(newUser, newUserAdded);
	    } else {
	    	//found a user. Check if passwords match
	    	if (user.password === password){
	    		res.send(user);
	    	} else {
	    		res.send("ERROR: username or password do not match for given user");
	    	}
	    }
	}

	if (username){
		//check if any registered user has the given username. If so login as the user if the password matches.
    	usersCollection.findOne({username: username.toLowerCase()}, checkUsername);
	}


});

module.exports = router;
