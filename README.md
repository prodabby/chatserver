This is a node.js server side implementation of a chat application. This code is just for illustration purposes.
In order to run this server, user must have node.js installed and also mongodb.
Also, you must do npm install to install all package dependencies for the application to work
type './bin/www' in command line in the directory of the server code.